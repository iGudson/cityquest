<?php

/**
 * This is the model class for table "{{main_page}}".
 *
 * The followings are the available columns in table '{{main_page}}':
 * @property integer $id
 * @property string $meta_description
 * @property string $meta_keywords
 * @property string $header
 * @property string $description
 * @property string $small
 * @property string $bottom
 * @property string $title
 * @property integer $city_id
 *
 * @property City $city
 */
class MainPage extends CActiveRecord
{


    /**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{main_page}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('city_id', 'numerical', 'integerOnly'=>true),
			array('title', 'length', 'max'=>255),
            array('city_id','unique','message'=>'Данный город уже используется.'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, meta_description, meta_keywords, header, description, small, bottom, title, city_id', 'safe'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'city' => array(self::BELONGS_TO, 'City', 'city_id'),
        );
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'meta_description' => 'Meta Description',
			'meta_keywords' => 'Meta Keywords',
			'header' => 'Заголовок',
			'description' => 'Описание',
			'small' => 'Маленький текст',
			'bottom' => 'Нижняя часть',
			'title' => 'Титул',
			'city_id' => 'Город',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('meta_description',$this->meta_description,true);
		$criteria->compare('meta_keywords',$this->meta_keywords,true);
		$criteria->compare('header',$this->header,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('small',$this->small,true);
		$criteria->compare('bottom',$this->bottom,true);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('city_id',$this->city_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return MainPage the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}


    /**
     * Возвращает объект с моделью страницы по указанному городу. Если
     * страницы не существует - возвращать Москву (id=1).
     * @param $city_id
     * @return static
     */
    public function getMainPageByCity($city_id)
    {
        $model = $this->findByAttributes(['city_id'=> $city_id]);
        if($city_id != City::DEFAULT_CITY && $model != null){
            $defaultModel = $this->findByAttributes(['city_id'=>City::DEFAULT_CITY]);
            $attributes = $model->getAttributes();
            foreach($attributes as $key => $value){
                if(empty($value)){
                    $model->$key = $defaultModel->$key;
                }
            }
        }
        return $model != null ? $model : $this->findByAttributes(['city_id' => City::DEFAULT_CITY]);
    }


}
