<?php

/**
 * This is the model class for table "{{event_log}}".
 *
 * The followings are the available columns in table '{{event_log}}':
 * @property integer $id
 * @property integer $user_id
 * @property integer $action
 * @property string $data
 * @property integer $create_at
 * @property string $model
 * @property integer $model_id
 */
class EventLog extends CActiveRecord
{
    const ACTION_ADD_BOOKING = 10;
    const ACTION_EDIT_BOOKING = 11;
    const ACTION_DELETE_BOOKING = 12;

    /**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{event_log}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_id, action', 'required'),
			array('user_id, action, create_at', 'numerical', 'integerOnly'=>true),

		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
        return array(
            'author' => array(self::BELONGS_TO, 'User', 'user_id'),
            );
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'user_id' => 'User',
			'action' => 'Action',
			'data' => 'Data',
			'create_at' => 'Create At',
		);
	}


	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return EventLog the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}





    /**
     * Добавляем в БД запись с новым ивентом. Само содержание будем хранить в json.
     * @param $action
     * @param $params
     * @param $model_name
     * @param $model_id
     * @internal param $model
     * @return bool
     */
    public static function addMsg($action, $params, $model_name, $model_id)
    {
        $model = new self;
        $data = json_encode($params);
        $model->data = $data;
        $model->action = $action;
        $model->user_id = \Yii::app()->user->id;
        $model->model = $model_name;
        $model->model_id = $model_id;
        return $model->save(false);
    }


    /**
     * Получаем содержание ивента из БД по ID.
     * @param $id
     * @return string
     */
    public static function getMsgById($id)
    {
        $data = self::model()->findByPk($id);
        return self::getMsg($data);
    }


    /**
     * Получаем содержание ивентов из БД по переданным данным (имени модели и ID модели).
     * @param $model
     * @param $model_id
     * @return array
     */
    public static function getMsgsByModelId($model,$model_id){
        $models = self::model()->findAllByAttributes(['model'=>$model,'model_id'=>$model_id]);
        $array = [];
        foreach($models as $one){
            $array[] = self::getMsg($one);
        }
        return $array;
    }


    /**
     * Возвращает сообщение по переданной модели.
     * @param $data
     * @return string
     */
    private function getMsg($data){
        $params = $data->data;
        $action = $data->action;
        $arrayAlias = [
            'model_id'=>$data->model_id
        ];
        $json_decode = self::doArray($params);
        if (!empty($json_decode['diff'])) {
            if (is_array($json_decode['diff'])) {
                $dataKeyValue = array();
                foreach ($json_decode['diff'] as $key => $value) {
                    $dataKeyValue[] = \Yii::t('EventLog',$key).': "'.$value.'"';
                }
                $json_decode['diff'] = implode(', ', $dataKeyValue);
            }
        }
        $alias = array_merge($json_decode,$arrayAlias);
        return \Yii::t('EventLog', $action, $alias);
    }

    /**
     * Приводим джсон в надлежащий вид.
     * @param $json
     * @return array|mixed
     */
    private static function doArray ($json) {
        $newArray = array();
        $array = json_decode($json, true);
        if (is_array($array)) {
            foreach ($array as $key => $value) {
                $newArray[$key] = self::doArray($value);
            }
            return $newArray;
        } elseif (empty($array)) {
            $array = $json;
        }

        return $array;
    }



    /**
     *  До сохранения присваиваем свойству create_at текущее время.
     */
    public function beforeSave()
    {
        if(parent::beforeSave())
        {
            if($this->isNewRecord)
                $this->create_at = time();
            return true;
        }
        else
            return false;
    }


    /**
     * Получаем датапровайдер по переданным данным, чтобы выбрать нужные логи
     * на определенное время, день и квест.
     * @param $quest
     * @param $date
     * @param $time
     * @return CActiveDataProvider
     */
    public function searchBookingHistory($quest,$date,$time)
    {
        $criteria=new CDbCriteria;

        $criteria->order = 'create_at DESC';
        $criteria->condition = "`tbl_booking`.`date` = :date AND `tbl_booking`.`time`= :time AND `tbl_booking`.`quest_id` = :quest_id";
        $criteria->params = [
            ':time'=>$time,
            ':date'=>$date,
            ':quest_id'=>$quest
        ];
        $criteria->join = "LEFT JOIN `tbl_booking` on `t`.`model_id` = `tbl_booking`.`id` JOIN `tbl_quest` on `tbl_booking`.`quest_id` = `tbl_quest`.`id`";

        $criteria->compare('id',$this->id);
        $criteria->compare('user_id',$this->user_id);
        $criteria->compare('action',$this->action);
        $criteria->compare('data',$this->data,true);
        $criteria->compare('create_at',$this->create_at);
        $criteria->compare('model',$this->model,true);
        $criteria->compare('model_id',$this->model_id);

        return new CActiveDataProvider($this, array(
            'pagination'=>array(
                'pageSize'=>9999,
            ),
            'criteria'=>$criteria,
        ));
    }
}
