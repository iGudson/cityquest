<table class='table borderless'><tbody>
<?

sort($times);
//check quest, who starts after midnight
preg_match('/^0[0-6]/', trim($times[0]), $check, PREG_OFFSET_CAPTURE);
$workAfterMidnight = !empty($check);

$questsInDay = count($times);

//wight quest's line, who starts from 00:00-05:59
$wightFirstLine = 0;

//wight quest's line, who starts from 06:00-17:59
$wightSecondLine = 0;

//wight quest's line, who starts from 18:00-23:59
$wightLastLine = 0;

foreach ($times as $key => $time) {
    $time = explode(':',$time);
    $hour = (int)$time[0];
    $minut = (int)$time[1];

    //delete quest, who starts from 01:00-07:59
    if ($hour > 0 && $hour < 8 && $model->schedule == '') {
        unset($times[$key]);
        continue;
    }

    if (($hour < 17 || $hour == 17 && $minut < 31) && $hour >= 6) {
        $wightSecondLine++;
    } elseif ($hour < 6) {
        $wightFirstLine++;
    } else {
        $wightLastLine++;
    }
}

$dashedFirstLine = $wightFirstLine > 1 ?'<span class="dashed">&nbsp;</span>' : '';
$dashedSecondLine = $wightSecondLine > 1 ?'<span class="dashed">&nbsp;</span>' : '';
$dashedLastLine = $wightLastLine > 1 ?'<span class="dashed">&nbsp;</span>' : '';

  $days = Yii::app()->params['days'];
  $month = Yii::app()->params['month'];
  $endDate = strtotime( '+'.Yii::app()->params['offset'].' day' );
  $currDate = strtotime( 'now' );
  $dayArray = array();

  function makeDayArray( ) {
    $days = array('понедельник', 'вторник', 'среда', 'четверг', 'пятница', 'суббота', 'воскресенье');
    $month = array('января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'ноября', 'октября', 'декабря' );
    $endDate   = strtotime( '+'.Yii::app()->params['offset'].' day' );
    $currDate  = strtotime( 'now' );
    $dayArray  = array();

    do{
      $dayArray[] = array(
        'day_name' => $days[intval(date( 'N' , $currDate ))-1],
        'month_name' => $month[intval(date( 'n' , $currDate ))-1],
        'day' => date( 'j' , $currDate ),
        'date' => date('Ymd', $currDate),
        'month' => date('m', $currDate),
        'year' => date('Y', $currDate),
      );
      $currDate = strtotime( '+1 day' , $currDate );
    } while( $currDate<=$endDate );

    return $dayArray;
  }

  $next_2week = makeDayArray();

foreach ($next_2week as $value) {

  if ( $value['day_name'] == 'суббота' ||  $value['day_name'] == 'воскресенье' || in_array($value['date'], $holidays)) {
    $workday = 0;
    $priceAm = $model->price_weekend_am;
    $pricePm = $model->price_weekend_pm;
  } else {
    $workday = 1;
    $priceAm = $model->price_am;
    $pricePm = $model->price_pm;
  }

  $price_original_Am = $priceAm;
  $price_original_Pm = $pricePm;

  if (isset($promo_days[$value['date']])) {
    $priceAm = $promo_days[$value['date']]->price_am;
    $pricePm = $promo_days[$value['date']]->price_pm;
  }
?>

	<tr>
		<td>
			<div class="curent_date <? echo !$workday ? 'weekend' : ''; ?>">
				<span class="quest_date"><em><?=$value['day']?>.</em><?=$value['month']?></span>
				<small><?=$value['day_name']?></small>
			</div>
			<? if (isset($promo_days[$value['date']])) { ?>
			<div class="curent_date" style="display:block;">
				<span class="promo-flag">Акция</span>
			</div> <? } ?>
		</td>
		<td>
			<table class='table borderless'><tbody>
				<tr>
					<? foreach ($times as $k=>$time) {

						$t = explode(':',$time);
						$hour = (int)$t[0];
						$minut = (int)$t[1];

						$price = (($hour < 17 || $hour == 17 && $minut < 31) && $hour != 0 && $workday)? $priceAm : $pricePm;

						$dis = 0;
						$near = 0;
						$time_str = $value['year'].'-'.$value['month'].'-'.$value['day'].' '.$time;
						$timastamp_quest_start = strtotime( $value['year'].'-'.$value['month'].'-'.$value['day'].' '.$time);
						if ( $timastamp_quest_start < (strtotime( 'now' )+ $model->time_preregistration ) ) $near = 1;

						$disabled = '';
						$my_quest = '';
						if ( isset($booking[$value['date']]) && isset($booking[$value['date']][$time]) ) {
						  $disabled = ' disabled="disabled"';
						  if ( $booking[$value['date']][$time]['competitor_id'] == Yii::app()->user->id ) {
						    $my_quest = ' myDate ';
						  }
						}

						?>
						<td>
							<div  type="button" 
							  data-name="<?=!Yii::app()->user->isGuest ? Yii::app()->getModule('user')->user()->username : ''?>" 
							  data-phone="<?=!Yii::app()->user->isGuest ? Yii::app()->getModule('user')->user()->phone : ''?>" 
							  data-time="<?=$time?>" 
							  data-quest="<?=$model->id?>" 
							  data-quest-cover="<?=$model->cover?>" 
							  data-ymd="<?=$value['date']?>" 
							  data-date="<?=$value['day']?> <?=$value['month_name']?>" 
							  data-day="<?=$value['day_name']?>" 
							  data-d="<?=$value['day']?>" 
							  data-m="<?=$value['month']?>" 
							  data-price="<?=$price?>" 
							  class="btn btn-q <?=$my_quest?>
							      <?=($near || $dis) ? 'disabled' : ''?>" 
							      <?=$disabled?>><?=$time?>
							</div>
						</td>
					<? } ?>
					</tr>
					<tr>
					<?

					$line_through = '';
					$promo_day = '';
					if (isset($promo_days[$value['date']]))
						$line_through = 'through';

					if ($workday) { ?>
						<? if ($workAfterMidnight) { // колонка для цены, для квеста у которого естсь сеанс после полуночи 00:*  ?>
							<td colspan="<?=$wightFirstLine?>">
								<div class="priceTbl priceTbl_onecol" title="Цена за команду" data-toggle="tooltip">
									<div class="priceRow">
                                        <?= $dashedFirstLine?>
										<span class="price" itemprop="price" content="<?=$price_original_Pm?>"><em class="<?=$line_through?>"><?=$price_original_Pm?></em> <?=$currency?></span>
                                        <?= $dashedFirstLine?>
                                    </div>
								</div>
								<? if (isset($promo_days[$value['date']])) { ?>
									<div class="priceTbl priceTbl_onecol" title="Цена за команду" data-toggle="tooltip">
										<div class="priceRow">
											<span class="price" itemprop="price" content="<?=$promo_days[$value['date']]->price_pm?>"><em><?=$promo_days[$value['date']]->price_pm?></em> <?=$currency?></span>
										</div>
									</div>
								<? } ?>
							</td>
						<? } ?>
						<td colspan="<?=$wightSecondLine?>">
							<div class="priceTbl" title="Цена за команду" data-toggle="tooltip">
								<div class="priceRow">
                                    <?= $dashedSecondLine?>
									<span class="price" itemprop="price" content="<?=$price_original_Am?>"><em class="<?=$line_through?>"><?=$price_original_Am?></em> <?=$currency?></span>
                                    <?= $dashedSecondLine?>
                                </div>
							</div>
							<? if (isset($promo_days[$value['date']])) { ?>
								<div class="priceTbl" title="Цена за команду" data-toggle="tooltip">
									<div class="priceRow">
                                        <span class="dashed">&nbsp;</span>
                                        <span class="price" itemprop="price" content="<?=$promo_days[$value['date']]->price_am?>"><em><?=$promo_days[$value['date']]->price_am?></em> <?=$currency?></span>
                                        <span class="dashed">&nbsp;</span>
									</div>
								</div>
							<? } ?>
						</td>
						<td colspan="<?=$wightLastLine?>">
							<div class="priceTbl" title="Цена за команду" data-toggle="tooltip">
								<div class="priceRow">
                                    <?= $dashedLastLine?>
									<span class="price" itemprop="price" content="<?=$price_original_Pm?>"><em class="<?=$line_through?>"><?=$price_original_Pm?></em> <?=$currency?></span>
									<?= $dashedLastLine?>
								</div>
							</div>
							<? if (isset($promo_days[$value['date']])) { ?>
								<div class="priceTbl" title="Цена за команду" data-toggle="tooltip">
									<div class="priceRow">
										<span class="dashed">&nbsp;</span>
										<span class="price" itemprop="price" content="<?=$promo_days[$value['date']]->price_pm?>"><em><?=$promo_days[$value['date']]->price_pm?></em> <?=$currency?></span>
										<span class="dashed">&nbsp;</span>
									</div>
								</div>
							<? } ?>
						</td>
					<? } else { ?>
						<td colspan="<?=$questsInDay?>" class="weekend">
							<div class="priceTbl" title="Цена за команду" data-toggle="tooltip">
								<div class="priceRow">
									<span class="dashed">&nbsp;</span>
									<span class="price" itemprop="price" content="<?=$price_original_Pm?>"><em class="<?=$line_through?>"><?=$price_original_Pm?></em> <?=$currency?></span>
									<span class="dashed">&nbsp;</span>
								</div>
							</div>
							
							<? if (isset($promo_days[$value['date']])) { ?>
								<div class="priceTbl " title="Цена за команду" data-toggle="tooltip">
									<div class="priceRow">
										<span class="dashed"></span>
										<span class="price" itemprop="price" content="<?=$promo_days[$value['date']]->price_pm?>">
										<?=$promo_days[$value['date']]->price_pm?> <?=$currency?>
										</span>
										<span class="dashed"></span>
									</div>
								</div>
							<? } ?>
						</td>


					<? } ?>
					</tr>
			</tbody></table>
		</td>
	</tr>

 <? } ?>
 </tbody></table>