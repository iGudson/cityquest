<?
$this->pageTitle= $page->title;

$this->description= $page->meta_description;

$this->keywords= $page->meta_keywords ;
?>
<div class="row rules">
	<div class="col-xs-10 col-xs-offset-1 col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2">
		<h1 class="h3 text-center">
		<?= $page->header ?>
		</h1>
		<?= $page->text ?>
	</div>

	<div style="display: none;" itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating">
		<meta itemprop="bestRating" content="5" />
		<meta itemprop="ratingValue" content="5" />
		<meta itemprop="ratingCount" content="11" />
	</div>
</div>