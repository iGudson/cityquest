<?php
/* @var $this RulesManagementController */
/* @var $model RulesModel */

?>
<div class="block">
    <div class="block-title">
        <h2>
            Добавить правила
            <small>
                <?= CHtml::link('<i class="hi hi-plus" aria-hidden="true"></i>',['create']) ?>
            </small>
        </h2>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <?=$this->renderPartial('_form', array('model'=>$model))?>
        </div>
        <div class="col-sm-12">&nbsp;</div>
    </div>
</div>m', array('model'=>$model)); ?>