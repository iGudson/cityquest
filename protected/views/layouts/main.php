<?php
//@todo спасите меня, если это некрасиво. Не знаю, куда еще запихнуть =(
$page = MainPage::model()->getMainPageByCity($this->city_model->id);
?>
<!DOCTYPE html>
<html lang="ru">
  
  <? include('head.php'); ?>

<body itemscope itemtype="http://schema.org/WebPage" class="<? if (Yii::app()->controller->id == 'quest' && Yii::app()->controller->action->id == 'index') echo ' video ';
    else echo ' inner ';
    ?>">
 <? if ($this->city_model->id != 2 ) { ?>
    <!-- Google Tag Manager -->
    <noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-MLLHVP"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-MLLHVP');</script>
    <!-- End Google Tag Manager -->
  <? } 

  if ( 0 && $_SERVER['HTTP_HOST'] != 'cq.il' && $_SERVER['HTTP_HOST'] != 'cq.kzil') { ?>
    <script type="text/javascript">
    (function (d, w) {
      w._admitadPixel = { response_type: 'img' };
      w._admitadPositions = w._admitadPositions || [];
      w._admitadPositions.push({
        uid: '',
        client_id: '',
        tariff_code: '1'
      });
      
      var id = '_admitad-pixel';

      if (d.getElementById(id)) { return; }

      var s = d.createElement('script');
      s.id = id;
      var r = (new Date).getTime();
      var protocol = (d.location.protocol === 'https:' ? 'https:' : 'http:');
      s.src = protocol + '//cdn.asbmit.com/static/js/pixel.min.js?r=' + r;
      d.head.appendChild(s);
     })(document, window)</script>

    <noscript>
      action_code: '1',
      campaign_code: 'e8be9dc3d4'
      order_id: '',
      payment_type: 'lead'
      <img src="//ad.admitad.com/r?uid=&campaign_code=e8be9dc3d4&action_code=1&response_type=img&order_id=&tariff_code=1&payment_type=lead" width="1" height="1" alt="">
    </noscript>
  <? } ?>
    
    <? include('nav.php'); ?>

    <div class="jumbotron">
      <? if (Yii::app()->controller->id == 'quest' && Yii::app()->controller->action->id == 'index') { ?>
      <div class="container text-center">
        <div class="row">
          <div class="col-md-10 col-md-offset-1 col-sm-12">
            <h1><?= $page->header ?></h1>
            <p class="lead-p"><?= $page->description ?></p>
            <small><?= $page->small ?></small>
            <p class="btns-filter">
              <span class="btn btn-filter">
                <i class="icon icon-logo_cityquest"></i> <span class="hidden-xs">Все квесты</span>
              </span>
              <span class="btn btn-filter active">
                <i class="icon icon-spiral"></i> <span class="hidden-xs">Обычные</span>
              </span>
              <span class="btn btn-filter">
                <i class="icon icon-alarm"></i> <span class="hidden-xs">Спортивные</span>
              </span>
              <span class="btn btn-filter">
                <i class="icon icon-star"></i> <span class="hidden-xs">Перфоманс</span>
              </span>
            </p>
          </div>
        </div>
      </div>
      <? } ?>
    </div>

		<?php echo $content; ?>
    
    <? if (Yii::app()->controller->id == 'quest' && Yii::app()->controller->action->id == 'index') { ?> 
      <div class="container-fluid">
        <div class="row">
          <div class="col-sm-10 col-sm-offset-1 col-xs-12">
              <p class="text-center bottom-lead">
                <?= $page->bottom ?>
              </p>
          </div>
        </div>
      </div>
    <? } ?>

    <? include('footer.php'); ?>
</body>
</html>