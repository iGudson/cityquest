<?php
/* @var $this MainPageController */
/* @var $model MainPage */

?>

<div class="block">
    <div class="block-title">
        <h2>
            <?=Yii::t('app','Management of cities')?>
            <small>
                <?= CHtml::link('<i class="hi hi-plus" aria-hidden="true"></i>',['create']) ?>
            </small>
        </h2>
    </div>
    <div class="row">
        <div class="col-sm-12">


<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'main-page-grid',
	'dataProvider'=>$model->search(),
    'cssFile'=>'',
    'htmlOptions'=>array('class'=>'table-responsive'),
    'itemsCssClass' => 'table table-striped table-responsive',
	'columns'=>array(
		'id',
//		'meta_description',
//		'meta_keywords',
        'header'=>array(
            'name'=>'header',
            'type'=>'html'
        ),
		'description'=>array(
            'name'=>'description',
            'type'=>'html'
        ),
		'small'=>array(
            'name'=>'small',
            'type'=>'html'
        ),

		'bottom'=>array(
            'name'=>'bottom',
            'type'=>'html'
        ),
		'title',
		'city_id'=>[
            'name'=>'city_id',
            'value'=>'$data->city->name'
        ],

		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
        </div>
    </div>
</div>

