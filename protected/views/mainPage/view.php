<?php
/* @var $this MainPageController */
/* @var $model MainPage */

?>

<div class="block">
    <div class="block-title">
        <h2>
            Добавить город
            <small>
                <?= CHtml::link('<i class="hi hi-plus" aria-hidden="true"></i>',['create']) ?>
            </small>
        </h2>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <?php $this->widget('zii.widgets.CDetailView', array(
                'data'=>$model,
                'attributes'=>array(
//		'id',
                    'meta_description',
                    'meta_keywords',
                    'header',
                    'description',
                    'small',
                    'bottom',
                    'title',
                    'city_id'=>[
                        'name'=>'city_id',
                        'value'=>$model->city->name
                    ],
                ),
            )); ?>
        </div>
        <div class="col-sm-12">&nbsp;</div>
    </div>
</div>

