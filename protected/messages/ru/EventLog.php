<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 16.06.2015
 * Time: 16:41
 */

return [
    EventLog::ACTION_ADD_BOOKING => 'Добавлена новая бронь #model_id',
    EventLog::ACTION_EDIT_BOOKING => 'Бронь #model_id была измена (old) на (new)',
    EventLog::ACTION_DELETE_BOOKING => 'Бронь #model_id была удалена. url_restore',
    'comment' => Yii::t('app','Comment'),
    'status' => Yii::t('app','Status'),
    'time' => Yii::t('app','Time'),
    'create_time' => Yii::t('app','Create Time'),
    'email' => Yii::t('app','Email'),
    'name' => Yii::t('app','Name'),
    'phone' => Yii::t('app','Phone'),
    'result' => Yii::t('app','Result'),
    'quest_id' => Yii::t('app','Quest'),
    'competitor_id' => Yii::t('app','Competitor'),
    'affiliate' => Yii::t('app','Affiliate'),
    'source' => Yii::t('app','Sources'),
    'discount' => Yii::t('app','Discounts'),
    'payment' => 'Метод оплаты',
    'winner_photo' => Yii::t('app','Winer photo'),
    'Restore'=>'Восстановить',
    'price'=>'Цена',
];