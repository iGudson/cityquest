$(window).click(function(e) {
    //console.log(e.target);
    if($(e.target).attr("id") === "popup-nav-manipulation") {
        var $popover = $(".popover");
        var popover_height_before = parseInt($popover.css("height"));
        var $nav = $("#popup-nav-tabs, #popup-nav-select");
        if ($nav.hasClass("hide-popup-nav")) {
            $nav.removeClass("hide-popup-nav");
            $(e.target).text("-");
        } else {
            $nav.addClass("hide-popup-nav");
            $(e.target).text("+");
        }
        var popover_height_after = parseInt($popover.css("height"));
        $(".popover .arrow").css("margin-top", parseInt($(".popover .arrow").css("margin-top")) - (popover_height_after - popover_height_before)*0.5);

    }
});


