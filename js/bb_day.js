var Holidays = Backbone.Collection.extend({
	initialize:function(options) {
		this.app = options.app;
		this.url = '/holiday/get?start='+options.start+'&end='+options.end;
		this.fetch({
			success:function(a,b,c){
				options.deferred_holidays.resolve(a);
			},	
			error:function(){}
		});
	},
	parse: function(response) {
		if (response && response.success) {
			return response.days;
		} else {
			return false;
		}
	}
});
var PromoDays = Backbone.Collection.extend({
	initialize:function(options) {
		this.app = options.app;
		this.url = '/promoDays/get?start='+options.start+'&end='+options.end;
		this.fetch({
			success:function(a,b,c){
				options.deferred_promo.resolve(a);
			},	
			error:function(){}
		});
	},
	parse: function(response) {
		if (response && response.success) {
			return response.days;
		} else {
			return false;
		}
	}
});

var DayView = Backbone.View.extend({
	className:'btn-group',
	template: _.template(
		'<a type="button" class="text-center btn btn-default btn-day">'+
			'<span><%= day %></span>'+
			'<small><%= dayOfTheWeek %></small>'+
			'<small><%= month %></small>'+
			'<span class="badge"><%= amount %></span>'+
		'</a>'
	),
	events:{
		"click":"activate",
	},
	initialize:function(){
		this.model.on('change', this.render, this);
		this.render();
	},
	render:function(){

		var day = this.model;

		this.$el
			.html( this.template(day.attributes) );

		var btn = $('a', this.$el)
			.attr( 'href', '#day/'+day.get('ymd') );


		if (day.get('active'))
			btn.addClass('btn-success').attr("disabled","disabled");
		else
			btn.removeClass('btn-success').removeAttr('disabled');


		if (day.get('today'))
			btn.addClass('active');
		else
			btn.removeClass('active');


		if (day.get('weekend') || day.get('holiday'))
			btn.addClass('btn-warning');
		else
			btn.removeClass('btn-warning');

		this.getBookings();

		return this;
	},


	getBookings:function(){

		var ymd = this.model.get('ymd'),
			m = this.model,
			v = this;

		$.get('/booking/getbyday?day='+ymd+'&city='+city_id, function(r){
			if (r.bookings.length != 0) {
				$('.badge', v.$el).html( r.bookings.length );
			}
		});

		setInterval(this.autoCheck, 30000, this.model);
	},

	autoCheck: function(d){
		var ymd = d.get('ymd'),
			m = d,
			v = d.view;

		$.get('/booking/getbyday?day='+ymd+'&city='+city_id, function(r){
			m.set('amount',r.bookings.length);
			if (r.bookings.length != 0) {
				$('.badge', v.$el).html( r.bookings.length );
			}
		})

		//setTimeout(d.autoCheck, 60000, d);
	},

	activate:function(){
		this.model.collection.removeActive();
		this.model.set('active',true);
		this.model.checkBookins();
	}
});

var Day = Backbone.Model.extend({
	defaults:{
		current:false,
		active:false,
		ymd:0,
		day:0,
		dayOfTheWeek:'',
		month:'',
		month_number:0,
		weekend:false,
		holiday:false,
		Y:2015,
		amount:0,
		m:00,
		d:00,
	},
	initialize:function(){
		this.view = new DayView({model:this});

		this.on('change:active change:holiday change:amount', function(){
			this.view.render();
		});

		this.on('destroy', this.modelDestroy, this)
	},

	modelDestroy:function(a,b,c){
		console.log(a,b,c);
	},

	/*getBookins:function(){
		var ymd = this.get('ymd'),
			m = this,
			v = this.view;

		$.get('/booking/getbyday?day='+ymd+'&city='+city_id, function(r){

			if (r.bookings.length != 0) {
				
				m.set('amount',r.bookings.length);

				$('.badge', m.view.$el).html( r.bookings.length );

			}
			
		});
	},*/
	checkBookins:function(){
		var ymd = this.get('ymd'),
			m = this,
			v = this.view;

		$.get('/booking/getbyday?day='+ymd+'&city='+city_id, function(r){
			
			m.set('amount',r.bookings.length);
			if (m.get('amount') != r.bookings.length) {
				console.log('setup bookings');
			}
		})
	},
	autoCheck: function(d){
		var ymd = d.get('ymd'),
			m = d,
			v = d.view;

		$.get('/booking/getbyday?day='+ymd+'&city='+city_id, function(r){
			m.set('amount',r.bookings.length);
			if (m.get('amount') != r.bookings.length) {
				console.log('setup bookings');
			}
		})

		/*q.bookings.fetch({success:function(collection){
			q.bookings.setupBookings();
		}});*/

		setTimeout(d.autoCheck, 20000, d);
	}
});

var Days = Backbone.Collection.extend({
	model: Day,
	days_of_week: ['Вс','Пн','Вт','Ср','Чт','Пт','Сб'],
	months: ['Янв','Фев','Мар','Апр','Май','Июн','Июл','Авг','Сен','Ноя','Окт','Дек'],
	period: 14,
	day_offset: -2,
	holidays_ready:false,
	promo_ready:false,
	initialize:function(models, options){
		this.app = options.app;
		this.deferred_holidays = $.Deferred();
		this.deferred_promo = $.Deferred();
		this.deferred = $.Deferred();

	},
	fill:function(day_offset){
		var date = new Date(),
			today_date = new Date(),
			active_date = new Date(),
			i = 0,
			self = this;

		if (day_offset == -2) {
			self.app.workspace.navigate("", {trigger: false});
		}

		this.each(function(m){
			m.view.remove();
		});
		this.reset();

		this.day_offset = day_offset || this.day_offset;

		date.setDate(date.getDate() + this.day_offset);

		while (i < this.period) {
			i++;
			var y = date.getFullYear(),
				m = ((date.getMonth()+1) < 10) ? ('0'+(date.getMonth()+1)) : date.getMonth()+1,
				d = (date.getDate() < 10 ) ? ('0'+date.getDate())  : date.getDate();

			var day = new Day({
				'dayOfTheWeek': this.days_of_week[date.getDay()],
				'day': date.getDate(),
				'month': this.months[date.getMonth()],
				'today': (today_date.getDate() == date.getDate()) ? true : false,
				'active': (active_date.getDate() == date.getDate()) ? true : false,
				'weekend':(date.getDay() == 0 || date.getDay() == 6) ? true : false,
				'Y': y,
				'm': m,
				'd': d,
				'ymd': y + '' + m + '' + d
			});

			// console.log(day.attributes, day.view.el);

			self.add(day);

			date.setDate(date.getDate() + 1);
		}

		models = this.models;

		this.render();
	},
	render:function(){
		var days_container = $('#bb_days .btn-group'),
			self = this;

		days_container.html('');

		$('<a class="text-center btn btn-default btn-day">'+
			'<span class="hi hi-chevron-left"></span>'+
		  '</a>')
				.click(function(){
					self.fill(self.day_offset - self.period);
				})
				.appendTo(days_container);

		this.each(function(model){
			// console.log(model.get('amount'));
			days_container.append(model.view.render().el);
		});
		
		$('<a class="text-center btn btn-default btn-day">'+
			'<span class="hi hi-chevron-right"></span>'+
		  '</a>')
				.click(function(){
					self.fill(self.day_offset + self.period);
				})
				.appendTo(days_container);

		
		var last_day = this.last(),
			first_day = this.first();

		this.holidays = new Holidays({
			start: first_day.get('ymd'),
			end: last_day.get('ymd'),
			app: self.app,
			deferred_holidays: self.deferred_holidays
		});

		this.promodays = new PromoDays({
			start: first_day.get('ymd'),
			end: last_day.get('ymd'),
			app: self.app,
			deferred_promo: self.deferred_promo
		});
	},
	markPromo:function(){
		var self = this;

		this.promodays.each(function(promo){
			var day = self.find(function(model) {
				return model.get('ymd') == promo.get('day');
			});
		});
		this.promo_ready = true;
	},
	markHolidays:function(){
		var self = this;

		this.holidays.each(function(promo){
			var day = self.find(function(model) {
				return model.get('ymd') == promo.get('holiday_date');
			});

			if (day){
				day.view.$el.attr('data-holiday',1);
				day.set('holiday',1);
			}
		});

		this.holidays_ready = true;
	},

	setDayOff: function(active_day){

		$('.today_is')
			.html(active_day.get('day')+' '+active_day.get('month')+'. '+active_day.get('Y'));


		$('.setHoliday')
			.attr({
				'data-date':active_day.get('ymd'),
				'data-holiday':(active_day.get('holiday') || active_day.get('weekend'))?1:0
			});

		if (active_day.get('holiday') || active_day.get('weekend')) {
			
			$('.setHoliday').addClass('hi-star').removeClass('hi-star-empty')
				.attr('title','Сделать рабочим');
			$('.block-title h2').attr('title','Выходной день');

		} else {
			
			$('.setHoliday').addClass('hi-star-empty').removeClass('hi-star')
				.attr('title','Сделать выходным');
			$('.block-title h2').attr('title','Рабочий день');

		}

		$('.setHoliday, .block-title h2')
			.tooltip('destroy')
			.tooltip({ container:'body'});
	},

	removeActive:function(){
		this.each(function(model){
			model.set('active',false);
		});
	},

	getActiveDate:function(){
		return this.find(function(m){
			return m.get('active');
		});
	},

	setDate: function(ymd){
		var self = this;

		this.each(function(model){
			if (model.get('ymd') == ymd) {
				model.set('active', true);
			} else {
				model.set('active', false);
			}
		});

		var active_day = self.getActiveDate();

		if ( typeof(active_day) == 'undefined' ) {
			self.fill(-2);
			self.app.workspace.navigate("", {trigger: false});
			active_day = self.getActiveDate();
		}

		if (this.holidays_ready && this.promo_ready){

			self.setDayOff(active_day);

			self.app.quests.each(function(quest){
				quest.setPrice(active_day);
			});

		} else {

			$.when( this.deferred_holidays, this.deferred_promo, self.app.quests.deferred ).done(function( holidays, promodays, quests ){
				
				holidays.app.days.markHolidays();
				holidays.app.days.setDayOff(active_day);
				promodays.app.days.markPromo();

				quests.each(function(quest){
					quest.seances.fetch({success:function(collection,response){
						if (response && response.success){
							quest.setPrice(active_day);
						}
					}});

					quest.setPrice(active_day);
				});

			});
		}
	}
});